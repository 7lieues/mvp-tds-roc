#!/bin/bash -i

if [ "$1" = "h" ] || [ "$1" = "-h" ] || [ "$1" = "--h" ] || [ "$1" = "help" ] || [ "$1" = "-help" ] || [ "$1" = "--help" ]; then
        echo -e "\nOptions possibles de ce script : "
        echo "${0} ARG1"
        echo "avec ARG1 = prodtds (ou tds) / pptds / dev (ou rien)"

        echo "Par défaut l'argument est ${0} dev"
        exit 0
fi

if [ "$1" = "prod" ] || [ "$1" = "production" ]; then
        SERVICE_CHOICE=production
        TAG=latest
        echo "On build pour la PROD"
elif [ "$1" = "kube" ] || [ "$1" = "k" ]; then
	SERVICE_CHOICE=kube
        TAG=kube
        echo "On build pour le KUBE, êtes-vous sur ?? vous êtes sur le Service TDS !!"
        sleep 5
elif [ "$1" = "preprod" ] || [ "$1" = "pptds" ] || [ "$1" = "pp" ]; then
        SERVICE_CHOICE=preproduction
        TAG=pptds
        echo "On build pour la PRE PRODUCTION"
elif [ "$1" = "tds" ] || [ "$1" = "prodtds" ] || [ "$1" = "templeduswing" ]; then
        SERVICE_CHOICE=tds
        TAG=tds
        echo "On build pour la PRODUCTION de TDS"
        
else
        SERVICE_CHOICE=development
        TAG=latest
        echo "On build pour l'env de dev local"
fi

echo "docker build . -t 7lieues/mvp-tds-roc:${TAG}"
docker build . -t 7lieues/mvp-tds-roc:${TAG}

if [ "$SERVICE_CHOICE" = "kube" ] || [ "$SERVICE_CHOICE" = "production" ] || [ "$SERVICE_CHOICE" = "tds" ] ; then
        DOCKER_HUB_USER=7lieues
        DOCKER_HUB_PASSWORD=pogkah-saqXap-8pogvy
        IMAGE_NAME=7lieues/mvp-tds-roc:${TAG}
        echo "docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD"
        docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
        echo "docker push $IMAGE_NAME"
        docker push $IMAGE_NAME
fi
