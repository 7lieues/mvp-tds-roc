ID=49


discipline_='{
"wcs": {"max": {"max": "220"},"cout_abonnement": {"cout_abonnement": "100"}, "expert": {"max": "80", "min": "55"}, "debutant": {"max": "60", "min": "50"}}, 
"bebop": {"max": {"max": "300"},"cout_abonnement": {"cout_abonnement": "500"}, "expert": {"max": "50", "min": "25"}, "debutant": {"max": "88", "min": "35"}}
}'


other_constraints_='{
"nb_max_creneaux":5, "nb_tot_eleve":200, "min_par_seance" :10, "nb_max_sol":3
}
'

teaching_slots_='{
     "s2": {"dispos": {"jeudi": ["20-21", "22-23"], "lundi": ["19-20", "20-21", "21-22"], "mardi": ["19-20", "20-21", "22-23"], "vendredi": ["19-20", "22-23"]}, "capa_max": {"capa_max" : ["400"]}, "cout_salle": {"cout_salle" : ["40"]}}
     }'

teachers_='{
"prof1": {"cout_prof": ["75"],"heures_min": ["1"], "dispo_jours": ["lundi", "jeudi", "mardi"], "dispo_salles": ["s2"], "dispo_disciplines": ["wcs_debutant", "wcs_expert", "bebop_expert"]},
"prof2": {"cout_prof": ["50"],"heures_min": ["1"], "dispo_jours": ["lundi", "vendredi"], "dispo_salles": ["s2"], "dispo_disciplines": ["wcs_debutant", "wcs_expert", "bebop_debutant"]}
 }'



echo -e "\n \n\n \n*************************************************************************************************"
echo -e "***********                SAVE JSON IN TDS_ROC_QUETIONS                         ****************"
echo -e "************************************************************************************************* \n \n\n \n"


echo "INSERT INTO tds_roc_questions(id,discipline,teachers,other_constraints,teaching_slots,inserted_at,updated_at) 
         VALUES ($ID,'$discipline_','$teachers_','$other_constraints_','$teaching_slots_',current_timestamp,current_timestamp)"
echo  "************************************************************************************************* "
echo  "************************************************************************************************* "



REQUEST=$( psql postgresql://postgresUsr:postgresPass@localhost:5432/mvp_customer_si_local \
         -c "INSERT INTO tds_roc_questions(id,discipline,teachers,other_constraints,teaching_slots,inserted_at,updated_at) 
         VALUES ($ID,'$discipline_','$teachers_','$other_constraints_','$teaching_slots_',current_timestamp,current_timestamp)"  )


echo -e "\n \n\n \n*************************************************************************************************"
echo -e "***********                       SEND ID TO MVP-TDS-ROC                           ****************"
echo -e "************************************************************************************************* \n \n\n \n"

curl --location --request POST 'localhost:8100/emploi' \
     --header 'Content-Type: application/json' \
      --data "{\"id\":  $ID  }"