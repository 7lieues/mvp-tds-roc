#start with the base Python 3.6 stretch exesting image,
FROM ubuntu:18.04
MAINTAINER Chedi Cherni "chedi@7lieues.io"




RUN apt-get update \
&& apt-get install python3 -y \
#python-setuptools \
python3-pip 

WORKDIR /app

RUN pip3 install numpy 
RUN pip3 install mip
RUN pip3 install xlwt
RUN pip3 install matplotlib
RUN pip3 install flask
RUN pip3 install flask_sqlalchemy

RUN pip3 install psycopg2-binary

RUN pip3 install PuLP

RUN apt-get install glpk-utils -y 
RUN apt-get install coinor-cbc -y

COPY . /app
#RUN ls /*
WORKDIR /app

RUN ls



ENTRYPOINT ["python3" ,"scripts/app.py" ]
#CMD [""]
