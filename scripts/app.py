from time import sleep
from flask import Flask, jsonify, request, json, make_response
import ro.main
import threading
import time

from flask import send_from_directory
from flask_sqlalchemy import SQLAlchemy
import models
import os

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=int(os.environ['LOG_LEVEL'])) #logging level in an enviroment variable

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


def Creation_emploi(INPUT):



    logger.info("\n\n#######      fetch tds_roc_levels        #############")
    logger.debug("\n\n##########     get levels order          #############")
    niveaux=db.session.query(models.levels) #  fetch tds_roc_levels
    niveaux_ordre={ i.name : i.numero for i in niveaux }
    logger.debug( niveaux_ordre )
    logger.debug("#############################################\n\n")

    question_id=INPUT["id"]
    logger.info("\n\n#######      INPUT RECIEVED        #############")
    logger.debug("\n\n#######      INPUT ID          #############")
    logger.debug(question_id)
    logger.debug("#############################################\n\n")

    #In models.py we map the table tds_roc_questions in the object models.Question 
    #The ORM SQLAlchemy use this mapping to do the sql request for us.
    row=db.session.query(models.Question).filter_by(id=question_id).first() #  GET THE QUESTION WITH THE RECIEVED ID FROM TABLE tds_roc_questions

    salles = row.teaching_slots
    for disc in salles.keys() :        #    FIX INPUT QUESTION SAME NESTING LEVEL PROBLEM
        salles[disc]["capa_max"] =   int(salles[disc]["capa_max"]["capa_max"][0] ) 


    logger.debug("#######      SEPERATED INPUT      ###########")
    logger.debug("#############################################")
    logger.debug(row.discipline)
    logger.debug("#############################################")
    logger.debug(row.teachers)
    logger.debug("#############################################")
    logger.debug(salles)
    logger.debug("#############################################")
    logger.debug(row.other_constraints)
    logger.debug("#############################################")
    logger.debug("#############################################\n\n")

    #PREPARE RO MODEL INPUT FROM DATA RECIEVED FROM THE DATABASE   
    json_contraintes={"discipline" :row.discipline  , "profs_" : row.teachers  , "other_constraints" :row.other_constraints  , "salle_" :salles   }

    logger.debug("\n\n########   FINAL MODEL INPUT     ##########")
    logger.debug(json_contraintes)
    logger.debug("#############################################\n\n")
    logger.info("\n\n********* Running RO model in background*******\n\n")
    time.sleep(2)

    data_json_answer=ro.main.emploi(json_contraintes,niveaux_ordre)      #MAIN.EMPLOI PREPARE VARIABLES AND LAUNCH RO MODEL

    logger.info("\n\n#############################################")
    logger.info("number of solutions :")
    logger.info(len( list(data_json_answer.keys())   ))
    logger.info("\n\n")
    logger.info("first solutions :")
    logger.info(f"first solutions : {data_json_answer['solution_numero_0'] }")
    logger.info("\n\n")

    #In models.py we map the table tds_roc_answers in the object models.answer 

    db.session.query(models.answer).filter_by(tds_roc_question_id=question_id).delete() #delete old answers for same id in tds_roc_answers 

    record=models.answer(   #prepare the object to save

        data=data_json_answer,
        tds_roc_question_id=question_id
    )

    db.session.add(record) 
    db.session.commit()    #save sql mapping of that object in tds_roc_answers

    logger.info (   "SOLUTIONS SAVED" )
    logger.debug("#############################################")
    logger.debug (   "RECORD ID={}".format(record.id) )
    logger.debug("#############################################")

    db.session.close()

@app.route("/ping")
def hello():

    logger.info("pong")
    return "pong"

@app.route('/emploi', methods=['POST'])
def emploi():

    t1 = threading.Thread(target=Creation_emploi,args=(request.get_json(),)) #create a background thread that will launch the function Creation_emploi(request.get_json())
    t1.start()

    return "ok"  #jsonify({"test":"200"})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0' , port=8100)




