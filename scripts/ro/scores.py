import logging
logger = logging.getLogger(__name__)

#Calcul du score de disparité par discipline(calculer la différence du nombre d'élèves entre les cours dans chaque discipline)
def score_disparité(List_filieres,list_res_tab,cour) :
            long_disc=[0]*(len(List_filieres)+1) #Initialisation de la varibale nécessaire pour séparer entre les cours selon le type de discipline
            score = [0]*len(list_res_tab) #Initialisation du score

            #Calcul de la variable séparente entre les disciplines 
            for d in range(len(List_filieres)):
                long_disc[d+1]=len(List_filieres[d])+long_disc[d] 


            for i in range(len(list_res_tab)):

              try :
                for j in range(len(list_res_tab[i])):
                  for k in range(len(list_res_tab[i])):
                    for d in range(len(List_filieres)):
                          #Vérification de l'appartenance des deux séance j et k au même type de discipline
                          if (j!=k) and (long_disc[d] <= cour.index(list_res_tab[i][j][1]) < long_disc[d+1]) and (long_disc[d] <= cour.index(list_res_tab[i][k][1]) < long_disc[d+1]):

                            score[i]+=abs(float(list_res_tab[i][j][4]) - float(list_res_tab[i][k][4])) #Addition de la différence en valeur absolue entre les cours de même discipline à la varibale score

              except :
                logger.error("SOLUTION NUM", i,  "ERRONE")

            #Affichage des résultats des scores trouvés pour les différentes solutions
            logger.debug( type(score))
            score = list(score)

            logger.debug("score_disparite  score")
            logger.debug(score)


            return(score)

#Calcul du score de contiguité(Trouver les séances contigues)
def score_contigue(List_filieres,list_res_tab,cour,niveaux_ordre) :

                long_disc=[0]*(len(List_filieres)+1 )  #Initialisation de la varibale nécessaire pour séparer entre les cours selon le type de discipline
                score = [0]*len(list_res_tab) #Initialisation du score
                
                #Calcul de la variable séparente entre les disciplines 
                for d in range(len(List_filieres)):
                    long_disc[d+1]=len(List_filieres[d])+long_disc[d] 


                score_cont = [0]*len(list_res_tab)
                score_prof = [0]*len(list_res_tab)
                score_disc = [0]*len(list_res_tab)



                for i in range(len(list_res_tab)):
                        for j in range(len(list_res_tab[i])-1):
                        ### score selon les créneaux contigus pour le même prof(Vérifier que les deux séances qui se suivent appartiennent au même jour et sont enseignées par le même prof et que leurs créneaux sont aussi contigus)
                            if list_res_tab[i][j][0]==list_res_tab[i][j+1][0] and list_res_tab[i][j][2]==list_res_tab[i][j+1][2] \
                                and (list_res_tab[i][j][3][0:3] ) == ( list_res_tab[i][j+1][3][0:3]) and int(list_res_tab[i][j][3][-1] ) +1 == int( list_res_tab[i][j+1][3][-1])  :
                                score_prof[i]+=1
                           
                           ####### score selon les cours contigus(Vérifier que les deux séances qui se suivent appartiennent à la même discipline, au même jour et leurs créneaux sont aussi contigus)
                            for d in range(len(List_filieres)):
                                if (long_disc[d] <= cour.index(list_res_tab[i][j][1]) < long_disc[d+1])  and (long_disc[d] <= cour.index(list_res_tab[i][j+1][1]) < long_disc[d+1]) \
                                     and (list_res_tab[i][j][3][0:3] ) == ( list_res_tab[i][j+1][3][0:3]) and int(list_res_tab[i][j][3][-1] ) +1 == int( list_res_tab[i][j+1][3][-1])  :
                        
                                    if  ( niveaux_ordre[ list_res_tab[i][j][1].split("_")[1] ] +1 == niveaux_ordre[ list_res_tab[i][j+1][1].split("_")[1]]  ):   #niveaux_ordre est utilisé pour vérifié si les deux niveau sont successifs 

                                      
                                        score_disc[i]+=1
                for i in range(len(score_cont)):
                    score_cont[i]=score_disc[i]+score_prof[i] #Somme des deux types de scores ci-dessus dans une variable "score_cont"

                #Affichage des résultats des scores trouvés pour les différentes solutions
                logger.debug("score_CONTIGUE  score")
                logger.debug(score_cont)

                return(score_disc, score_prof,   score_cont)

