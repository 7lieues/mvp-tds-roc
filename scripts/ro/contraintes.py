def CN(Profs_Jours_Salles_Disc,Profs_Jours_Salles_Disc_,Profs,Salles,Salles_Jours,Profs_Disc, Disciplines, List_filieres, cout_salles, cout_prof):
    import numpy as np
    from copy import deepcopy
    
    #C1:Un prof Profs(i) peut donner au plus un cours pendant l’horaire H(k) dans une salle S(j):
    C1=np.zeros(((len(Profs)*len(Salles_Jours[0]) , (len(Profs_Jours_Salles_Disc)))))
    mult=len(Salles)*len(Salles_Jours[0])*len(Profs_Disc[0])
    mult_1=len(Profs_Disc[0])*len(Salles_Jours[0])
    mult_2=len(Profs_Disc[0])
    ind1=0
    while (ind1<(len(Profs)*len(Salles_Jours[0]))):
        for i in range(len(Profs)):
            for j in range(len(Salles_Jours[0])):
                for l in range(len(Salles)):
                    C1[ind1][i*mult+j*mult_2+l*mult_1:i*mult+j*mult_2+l*mult_1+mult_2]=Profs_Jours_Salles_Disc[i*mult+j*mult_2+l*mult_1:i*mult+j*mult_2+l*mult_1+mult_2]
                ind1+=1
                
    #C2:Une salle S(j) peut contenir au plus un cours C(l) donné par un seul prof pendant l’horaire H(k):
    C2=np.zeros(((len(Salles)*len(Salles_Jours[0]),(len(Profs_Jours_Salles_Disc)))))
    ind2=0
    while(ind2<len(Salles)*len(Salles_Jours[0])):
        for i in range(len(Salles)):
            for j in range(len(Salles_Jours[0])):
                for k in range(len(Profs)):
                    C2[ind2][i*mult_1+j*mult_2+k*mult:i*mult_1+j*mult_2+k*mult+mult_2]=Profs_Jours_Salles_Disc[i*mult_1+j*mult_2+k*mult:i*mult_1+j*mult_2+k*mult+mult_2]
                ind2+=1
    #C3:Le nombre maximal d’élèves doit être inférieur à la capacité maximale des salles :
    C3=np.zeros((1,len(Profs_Jours_Salles_Disc)))
    for  i in range(C3.shape[1]):
        C3[0][i]=Profs_Jours_Salles_Disc[i]

    #C4_1:Chaque discipline doit être présente
    C4_1=np.zeros((len(Disciplines),len(Profs_Jours_Salles_Disc)))
    ind3=0
    while ind3 < (len(Disciplines)):
        for k in range(len(Profs)):
            for i in range(len(Salles)):
                for j in range(len(Salles_Jours[0])):
                    C4_1[ind3][ind3+mult*k+i*mult_1+j*mult_2]=Profs_Jours_Salles_Disc[ind3+mult*k+i*mult_1+j*mult_2]
        ind3+=1
        
    C4_1 = -C4_1
    #C4_2:Chaque prof doit enseigner un minimum dheures
    C4_2=np.zeros((len(Profs),len(Profs_Jours_Salles_Disc)))
    for i in range(len(Profs)):
        C4_2[i][i*mult:(i+1)*mult]=Profs_Jours_Salles_Disc[i*mult:(i+1)*mult]
        
    C4_2 = -C4_2

    #Le nombre d'élèves d'ensembles de disciplines est limité
    C_Max_Disc=np.zeros((len(List_filieres),len(Profs_Jours_Salles_Disc)))
    long_disc=[0]*(len(List_filieres)+1)

    for d in range(len(List_filieres)):
        long_disc[d+1]=len(List_filieres[d])+long_disc[d]
        for i in range(len(Profs)):
            for j in range(len(Salles)):
                for k in range(len(Salles_Jours[0])):
                    C_Max_Disc[d][i*mult+j*mult_1+k*mult_2+long_disc[d]:i*mult+j*mult_1+k*mult_2+long_disc[d+1]]=Profs_Jours_Salles_Disc[i*mult+j*mult_1+k*mult_2+long_disc[d]:i*mult+j*mult_1+k*mult_2+long_disc[d+1]]

    #Le nombre d'élèves doit être inférieur à la capacité de la salle à chaque créneau (nécessaire pour comparer ensuite avec la limite du nombre d'élèves dans chaque cours)
    b6=np.zeros((int(len(Profs_Jours_Salles_Disc_)/len(Profs))))
    
    for i in range(len(Salles)):
        b6[i*mult_1:i*mult_1+mult_1]=Salles[i]
    b6_=deepcopy(b6)
    for i in range(len(Profs)-1):
        b6_=np.concatenate((b6_,b6))
    
     ###############   couts des salles

    cost_salle=np.zeros((int(len(Profs_Jours_Salles_Disc)/len(Profs))))
    for i in range(len(cout_salles)):
        cost_salle[i*mult_1:i*mult_1+mult_1]=cout_salles[i]
    cost_salles=deepcopy(cost_salle)
    for i in range(len(Profs)-1):
        cost_salles=np.concatenate((cost_salles,cost_salle))

    ###############   couts des profs

    cost_prof=np.zeros((len(Profs_Jours_Salles_Disc)))
    for i in range(len(cout_prof)):
        cost_prof[i*mult:i*mult+mult]=cout_prof[i]

    C_budget=np.zeros((1,len(Profs_Jours_Salles_Disc)))
    for  i in range(C_budget.shape[1]):
        C_budget[0][i]=Profs_Jours_Salles_Disc[i]*(cost_salles[i]+cost_prof[i])

    return(C1, C2, C3, C4_1, C4_2, C_Max_Disc, b6_, C_budget)
