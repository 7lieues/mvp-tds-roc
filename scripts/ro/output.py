import logging
logger = logging.getLogger(__name__)

  


def createXML(list_res_tab,Salles_Jours,Jours_c,Salles_s_res,score_disparite,score_contigue,score_disc,score_prof,emplois_path) :
            import xlwt 
            from xlwt import Workbook 

            wb = Workbook()
            res0 = wb.add_sheet('resultat0') 
            
            #Style d'écriture
            style_l = xlwt.easyxf('font: bold 1, color red;') 
            style_c = xlwt.easyxf('font: bold 1, color blue;') 
            style_sol = xlwt.easyxf('font: bold 1, color green;')



            for i in range(len(list_res_tab)):


                    res0.write(i*(len(Salles_Jours)+1) , 0 , 'Sol'+str(i+1),style_sol)#+':'+str(cout_sol[i])+'€' ,style_sol)
                    res0.write(i*(len(Salles_Jours)+1) , len(Jours_c)+4 , 'Score_Disparité', style_sol)
                    res0.write(i*(len(Salles_Jours)+1)+1 , len(Jours_c)+4 , str(score_disparite[i]))

                    res0.write(i*(len(Salles_Jours)+1) , len(Jours_c)+1 , 'Score_Prof', style_sol)
                    res0.write(i*(len(Salles_Jours)+1) , len(Jours_c)+2, 'Score_Discipline', style_sol)
                    res0.write(i*(len(Salles_Jours)+1) , len(Jours_c)+3, 'Score_contigue', style_sol)
                    res0.write(i*(len(Salles_Jours)+1)+1 , len(Jours_c)+1 , str(score_prof[i]))
                    res0.write(i*(len(Salles_Jours)+1)+1 , len(Jours_c)+2 , str(score_disc[i]))
                    res0.write(i*(len(Salles_Jours)+1)+1 , len(Jours_c)+3 , str(score_contigue[i]))

                    for k in range(len(Jours_c)):
                        res0.write(i*(len(Salles_Jours)+1),k+1,Jours_c[k],style_l)

            for i in range(len(list_res_tab)):
                for j in range(len(Salles_s_res)):
                    res0.write((i*(1+len(Salles_s_res)))+j+1,0,'Sol'+str(i+1)+'_'+Salles_s_res[j],style_c)



            for i in range(len(list_res_tab)):
                try :

                    for l in range(len(list_res_tab[i])):
                        ligne=Salles_s_res.index(list_res_tab[i][l][2])
                        colonne=Jours_c.index(list_res_tab[i][l][3])
                        res0.write((i*(1+len(Salles_s_res)))+ligne+1,colonne+1,list_res_tab[i][l][0]+' '+list_res_tab[i][l][1]+' '+list_res_tab[i][l][4])
                except :
                    # list_res_tab.pop(i)
                    # score.pop(i)
                    break
                    logger.error("***********************************************")
                    logger.error(i)
                    logger.error("***********************************************")
                    logger.error("solution invalide  ( c1 ou c2 non respecter )!")

            wb.save(emplois_path) 





def createJSON(list_res_tab,score_disparite,score_contigue,nb_eleves_discipline,vfo,nbh_par_fil,List_filieres,nbh_total,cout_sol,revenue_sol):  # Put solution list (list_res_tab) with diffrent scores in proper output json


    json_temp=[]
    k=0
    for solution in (list_res_tab):
        solution_=[]     
        try :
            for seance in solution : 

                seance_=  {"creneau" : seance[3]  , "salle" : seance[2]  ,  "enseignant" : seance[0 ] , "cour" : seance[1]  ,"nb_eleves" :  float( seance[4]) }  # add each information name                
                solution_.append(seance_) 


            List_filieres=list(List_filieres)

            nbh_par_fil_={ List_filieres[i]  :nbh_par_fil[k][i] for   i in range(len(List_filieres)) }
            nb_eleves_discipline_={ List_filieres[i]  :nb_eleves_discipline[k][i] for   i in range(len(List_filieres)) }
            
            json_temp.append(    { "score_contigue": score_contigue[k],"score_disparite":score_disparite[k],"cout_sol": cout_sol[k] ,"revenue_sol" : revenue_sol[k] ,"nb_heures_discipline": nbh_par_fil_ ,
                          "nb_heures_total":nbh_total[k], "nb_eleves_discipline":nb_eleves_discipline_,"nb_eleves_total" : vfo, "solution":solution_}       ) # add scores.
        
        except :
            logger.error("solutionS invalide  (none)!")

        k=k+1

    data_json_answer = {f"solution_numero_{k}":v for (k,v) in enumerate(json_temp)}   #CONVERT LIST TO DICT TO SEND AS JSON OUTPUT

    return(data_json_answer)


