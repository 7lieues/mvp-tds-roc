import logging


logger = logging.getLogger(__name__)


import ro.contraintes
import ro.scores
import ro.output

def emploi(json_contraintes,niveaux_ordre):
    #!/usr/bin/env python3
  # -*- coding: utf-8 -*-
  """
  Created on Mon Jul 27 15:30:09 2020

  @author: chedi
  """
  import os
  emplois_path = os.environ['EMPLOIS_PATH'] 
  histogramme_path = os.environ['HISTOGRAMME_PATH'] 
  # savedir = saved_files/emploi.xml
  
  ############################
  #
  # 1 - INITIALISATION DES VARIABLES
  #
  ############################


  Jours=["lundi","mardi","mercredi","jeudi","vendredi"] # Liste des jours de la semaine en strings


  other_constraints=json_contraintes["other_constraints"]

  # nb_max_cr=int ( other_constraints["nb_max_creneaux"] ) #Le nombre des créneaux dans chaque jour de la semaine : un entier
  nb_tot_eleve = int ( other_constraints["nb_tot_eleve"] ) #Nombre totale d'élèves à enseigner
  min_par_seance = int ( other_constraints["min_par_seance"] ) #Nombre minimal d'élèves à enseigner dans chaque séance
  nb_max_sol=int ( other_constraints["nb_max_sol"] ) #Nombre maximal de solutions(d'emplois de temps)
  
  try :
    budget = float ( other_constraints["budget"] )
  except :
    budget = 0


  try :
    nb_semaines = int ( other_constraints["nb_semaines"] )#32
  except :
    nb_semaines = 32

  #Remplissage des variables cout_salles, liste des salles, capacité des salles, disponibilité des salles à partir des données json
  # type_tri=json_contraintes["type_tri"] 
  cout_salles=[]

  salle_json=json_contraintes["salle_"]
  salle_=list(salle_json.keys())

  s_dispo=[]
  cap_salle=[]
  for sal in salle_json.values():

    cap_salle.append(  int(sal["capa_max"] ) )

    jours_disp= sal["dispos"].keys()

    try :                     #if cout_salle given in input json we take it else we append 0
        cout_salles.append(      int(sal["cout_salle"]["cout_salle"][0])       )
    except:
        cout_salles.append(    0      )

    temp=[]
    for j in jours_disp:
        crens= sal["dispos"][j]
        for cren in crens :
           temp.append( j+"_"+cren)


    s_dispo.append(temp)



  logger.debug("\n\n************************************************************")
  logger.debug("************************************************************")
  logger.debug("les salles :")
  logger.debug(salle_)
  logger.debug("couts des salles")  
  logger.debug(cout_salles)  
  logger.debug("capacité des salles")  
  logger.debug(cap_salle)  
  logger.debug("disponibilité des salles")
  logger.debug(s_dispo)
  logger.debug("************************************************************")
  logger.debug("************************************************************")


  
  Salles=cap_salle #Capacité des salles respectivement à leur noms : des entiers
  Salles_s=salle_ #Liste des noms des salles en string



  ############################################################################
  #Remplissage des variables disciplines, nbre max d'élèves par cours et par disciplines, nbre min d'élèves par discipline et liste des disciplines à partir des données json
  discipline_json=json_contraintes["discipline"]



  #################################### Apporté les niveau des disciplines a partir des disponibilité des profs 
  profs_=json_contraintes["profs_"]

  list_disciplines_cours=[]
  for prof in profs_.values():

    for j in prof["dispo_disciplines"] :
        if j not in list_disciplines_cours :
           list_disciplines_cours.append(j)


  for i in list_disciplines_cours :
    dics_seul = i.split("_")[0]
    cour_seul =i.split("_")[1]

    try :
      if ( cour_seul  not in  list( discipline_json[dics_seul].keys() )  ) :
          discipline_json[dics_seul][cour_seul] = {} 
    except :
       logger.debug("don't exist")


  logger.debug("\n\n************************************************************")
  logger.debug("discipline_json apres ajout niveaux ")
  logger.debug(discipline_json)
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  #######################################



  rev_elev_disc=[]

  discs_=discipline_json.keys()

  cours_=[]
  max_par_cour = []
  max_par_disc = []
  min_par_cour=[]
  list_disciplines_=[]

  for disc_ in discipline_json.keys() :
      # try : 
      max_par_disc.append (  int(discipline_json[disc_]["max"])   )
      # except :
      #     max_par_disc.append(  nb_tot_eleve )

      try : 
          rev_elev_disc.append (  int(discipline_json[disc_]["cout_abonnement"]["cout_abonnement"])   )
      except :
          rev_elev_disc.append(  0 )



      temp_l=[]

      for c in list(   discipline_json[disc_].keys()   ):
        if c !="max" and c !="min"  and c!= "cout_abonnement":
          
          try : 
             max_par_cour.append(int(   discipline_json[disc_][c]["max"]     )) 
          except :
             max_par_cour.append(  nb_tot_eleve )


          try : 
             min_par_cour.append(int(   discipline_json[disc_][c]["min"]     )) 
          except :
             min_par_cour.append(  0 )


          cours_.append(  disc_+"_"+c    )
          temp_l.append(  disc_+"_"+c    ) 
          

      list_disciplines_.append(temp_l)
  

  logger.debug("\n\n************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("liste des cours")
  logger.debug(cours_)
  logger.debug("list_disciplines_")
  logger.debug(list_disciplines_)
  logger.debug("max_par_cour")
  logger.debug(max_par_cour)
  logger.debug("min_par_cour")
  logger.debug(min_par_cour)
  logger.debug("max_par_disc")
  logger.debug(max_par_disc)
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")

  cour=cours_#Liste des noms des cour à enseigner en string
  List_filieres=list_disciplines_

  Disc=max_par_cour #La limite du nombre d'élèves pour chaque cours selon la matière (ou discipline) : des entiers
  nbr_min_éléve = min_par_cour #La limite minimale du nombre d'élèves pour chaque cours selon la matière (ou discipline) : des entiers
 
  max_Filiere=max_par_disc

  #############################################
  #Remplissage des variables cout_prof, disponibilité des profs dans les salles, dans les jours et le nbre min d'heures à ensigner pour les profs à partir des données json

  cout_prof=[]


  profs_=json_contraintes["profs_"]
 
  p_dispo_salles=[]
  p_dispo_cours=[]
  p_dispo_jours=[]
  Heures_min_profs_=[]
  for prof in profs_.values():
    p_dispo_jours.append(prof["dispo_jours"])
    p_dispo_salles.append(prof["dispo_salles"])

    try :                     #if cout_prog given in json we take it else we append 0
        Heures_min_profs_.append(      int(prof["heures_min"][0])       )
    except:
        cout_prof.append(    0      )

   

    p_dispo_cours.append(prof["dispo_disciplines"])

    try :                     #if cout_prog given in json we take it else we append 0
        cout_prof.append(      int(prof["cout_prof"][0])       )
    except:
        cout_prof.append(    0      )




  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug(  " list_profs "  )
  logger.debug(   list(profs_.keys())  )
  logger.debug("disponibilité en jours")
  logger.debug(p_dispo_jours)
  logger.debug("disponibilité en salles")
  logger.debug(p_dispo_salles)
  logger.debug("disponibilité en cours")
  logger.debug(p_dispo_cours)
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")
  logger.debug("************************************************************")


  Profs=list(profs_.keys())
  Heures_min_profs=Heures_min_profs_



  ############################
  #
  # 2 - AFFECTATION DES VARIABLES
  #
  ############################

  cren_string = []
  for j in s_dispo :
      for i in j :
        if (i[-5:] not in cren_string ) :
           cren_string.append(i[-5:])


  tri_list = [ int (i[:2])  for i in  cren_string]
  

  tri_list, cren_string = zip(*sorted(zip(tri_list, cren_string)))

  nb_max_cr = len(tri_list)
  
  #Génération d'une liste contenant Jour+Créneau
  Jours_c=[]
  for i in (Jours):
    for j in (cren_string):
      Jours_c.append(i+"_"+j)
  #logger.debug(Jours_c)
  import numpy as np
  
  logger.debug("*============================================================================")
  logger.debug("cren_string")
  logger.debug(cren_string)

  logger.debug("tri_list")
  logger.debug(tri_list)
  logger.debug("Jours_c")
  logger.debug(Jours_c)
  logger.debug("============================================================================*")
  
  #Initialisation du tableau Salle_Jours(les salles en lignes et Jours+Crénaux en colonnes) représentant la disponibilité des salles selon les créneaux pendant les jours de la semaine
  Salles_Jours= np.zeros((len(Salles_s),len(Jours)*nb_max_cr))
  Salles_Jours = Salles_Jours.tolist()


  #Remplissage du tableau ci-dessus à partir des données
  for k in range(len(Salles_s)):
    for i in (s_dispo[k]):
      for j in range(len(Jours_c)):
        if i==Jours_c[j]:
          Salles_Jours[k][j]=1
    
  #Initialisation du tableau Profs_Jours(les Profs en lignes et Jours en colonnes) représentant la disponibilité des Profs selon les jours de la semaine
  Profs_Jours= np.zeros((len(Profs),len(Jours)))
  Profs_Jours = Profs_Jours.tolist()

  #Remplissage du tableau ci-dessus à partir des données
  for k in range(1,len(Profs)+1):
    for i in (p_dispo_jours[k-1]):
      for j in range(len(Jours)):
        if i==Jours[j]:
          Profs_Jours[k-1][j]=1

  #Initialisation du tableau Profs_Salles(les Profs en lignes et Salles en colonnes) représentant la disponibilité des Profs selon les Salles
  Profs_Salles= np.zeros((len(Profs),len(Salles_s)))
  Profs_Salles = Profs_Salles.tolist()

  #Remplissage du tableau ci-dessus à partir des données
  for k in range(1,len(Profs)+1):
    for i in (p_dispo_salles[k-1]):
      for j in range(len(Salles_s)):
        if i==Salles_s[j]:
          Profs_Salles[k-1][j]=1
  
  #Initialisation du tableau Profs_Disc(les Profs en lignes et cour en colonnes) représentant la disponibilité des Profs selon les cour
  Profs_Disc= np.zeros((len(Profs),len(Disc)))
  Profs_Disc = Profs_Disc.tolist()

  #Remplissage du tableau ci-dessus à partir des données
  for k in range(len(Profs)):
    for dispo in (p_dispo_cours[k]):
      for j in range(len(cour)):
        if dispo==cour[j]:
          Profs_Disc[k][j]=1

  #Initialisation du tableau Profs_Disc_(les Profs en lignes et cour en colonnes) représentant la disponibilité des Profs selon les cour avec la valeur maximale d'élèves en chacune d'elles
  Profs_Disc_= np.zeros((len(Profs),len(cour)))
  Profs_Disc_ = Profs_Disc_.tolist()

  #Remplissage du tableau ci-dessus à partir des données
  for k in range(1,len(Profs)+1):
    for i in (p_dispo_cours[k-1]):
      for j in range(len(cour)):
        if i==cour[j]:
          Profs_Disc_[k-1][j]=Disc[j]

  #Affectation d'étiquettes pour les variables plus tard contenant chacune les noms du prof, discipline, salle et le créneau dans le jour associé
  Tag=[]

  prof=0
  while(prof<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag.append(Profs[prof]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+str(k)+" ")
    prof+=1  

  Tag_y=[] 

  prof_=0
  while(prof_<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag_y.append(Profs[prof_]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+" "+str(k)+"_"+" ")
    prof_+=1  
  ###################################### pulp ##############################


  Tag_xw=[]
  prof_=0
  while(prof_<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag_xw.append("xw"+"_"+Profs[prof_]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+str(k)+"_"+" ")
    prof_+=1  

  Tag_xalpha=[]
  prof_=0
  while(prof_<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag_xalpha.append("xalpha"+"_"+Profs[prof_]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+str(k)+"_"+" ")
    prof_+=1  

  Tag_yw=[]
  prof_=0
  while(prof_<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag_yw.append("yw"+"_"+Profs[prof_]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+str(k)+"_"+" ")
    prof_+=1  

  Tag_yalpha=[]
  prof_=0
  while(prof_<len(Profs)):
    for j in range(len(Salles_s)):
      for i in range(len(Jours)):
        for k in range(1,nb_max_cr+1):
          for d in range (len(cour)):
            Tag_yalpha.append("yalpha"+"_"+Profs[prof_]+" "+cour[d]+" "+Salles_s[j]+" "+Jours[i]+str(k)+"_"+" ")
    prof_+=1  


###########################################################################################

  #Linéarisation des données (aplatir les dimensions pour l'obtention d'un système linéaire)

  #Combiner Profs_Salles avec Salles_Jours : On va obtenir un nouveau tableau "Profs_Jours_Salles" (Profs en lignes et salles avec jours en colonnes) représentant la disponibilité des profs selon les salles dans chaque jour
  from copy import deepcopy
  Profs_Jours_Salles=deepcopy(Profs_Salles)
  for i in range(len(Profs_Jours_Salles)):
    for j in range(len(Profs_Jours_Salles[i])):
      if Profs_Jours_Salles[i][j]!=0:
        Profs_Jours_Salles[i][j]=Salles_Jours[j] 
      else:
        Profs_Jours_Salles[i][j]=[0]*len(Jours_c)

  #Convertir le tableau généré de 3 dimensions en 2 seulement
  for i in range(len(Profs_Jours_Salles)):
    Profs_Jours_Salles[i]=sum(Profs_Jours_Salles[i],[])


  #Combiner Profs_Salles_Jours avec Profs_Jours : Vérifier si le prof est disponible le jour choisi sinon remplacer toutes les valeurs de ce jour par 0
  for i in range(len(Profs_Jours)):
    for j in range(len(Profs_Jours[i])):
      if Profs_Jours[i][j]==0:
        for k in range(nb_max_cr):
          for m in range(len(Salles_s)):
            Profs_Jours_Salles[i][m*len(Jours_c)+nb_max_cr*j+k]=0
          
  #Combiner Profs_Salles_Jours avec Profs_cour : Tableau de profs en lignes représentant leur disponibilté selon les salles et les créneaux dans chaque jour avec les cour aussi (nombre de colonnes sera nb_salles*nb_jours*nb_créneaux*nb_cour)  
  Profs_Jours_Salles_Disc=deepcopy(Profs_Jours_Salles)
  for i in range(len(Profs_Jours_Salles_Disc)):
    for j in range(len(Profs_Jours_Salles_Disc[i])):
      if Profs_Jours_Salles_Disc[i][j]!=0:
        Profs_Jours_Salles_Disc[i][j]=Profs_Disc[i]
      else:Profs_Jours_Salles_Disc[i][j]=[0]*len(cour)

  #Convertir le tableau généré de 3 dimensions en 2 seulement
  for i in range(len(Profs_Jours_Salles_Disc)):
    Profs_Jours_Salles_Disc[i]=sum(Profs_Jours_Salles_Disc[i],[])
  
  Profs_Jours_Salles_Disc=sum(Profs_Jours_Salles_Disc,[])

  #Combiner Profs_Salles_Jours avec Profs_cour_ max_cour : Même tableau que ci-dessus avec la valeur de la limite du nombre d'élèves pour chaque cours au lieu de "1"
  Profs_Jours_Salles_Disc_=deepcopy(Profs_Jours_Salles)
  for i in range(len(Profs_Jours_Salles_Disc_)):
    for j in range(len(Profs_Jours_Salles_Disc_[i])):
      if Profs_Jours_Salles_Disc_[i][j]!=0:
        Profs_Jours_Salles_Disc_[i][j]=Profs_Disc_[i]
      else:Profs_Jours_Salles_Disc_[i][j]=[0]*len(cour)

  #Convertir le tableau généré de 3 dimensions en 2 seulement
  for i in range(len(Profs_Jours_Salles_Disc_)):
    Profs_Jours_Salles_Disc_[i]=sum(Profs_Jours_Salles_Disc_[i],[])

  Profs_Jours_Salles_Disc_=sum(Profs_Jours_Salles_Disc_,[])
  
  ############################
  #
  # 3 - IMPORT DES CONTRAINTES
  #
  ############################


  #Importation des contraintes
  C1, C2, C3, C4_1, C4_2, C_Max_Disc, b6_, C_budget = ro.contraintes.CN(Profs_Jours_Salles_Disc,Profs_Jours_Salles_Disc_,Profs,Salles,Salles_Jours,Profs_Disc, cour, List_filieres, cout_salles, cout_prof)

  #Matrice des contraintes finale
  Mat_Cont_y=np.concatenate((C1, C2))
  Mat_Cont_y=np.concatenate((Mat_Cont_y, C4_2))
 
  Mat_Cont=np.concatenate((C3,C4_1))
  Mat_Cont=np.concatenate((Mat_Cont,C_Max_Disc))

  #Suprression des colonnes nulles Mat_cont
  supp=[]
  for i in range(Mat_Cont.shape[1]):
      if (Mat_Cont[:,i]==np.zeros(Mat_Cont.shape[0])).all():
        supp.append(i)

  Mat_Cont=np.delete(Mat_Cont,supp, axis=1)

  #Suprression des colonnes nulles Mat_cont_y
  supp_y=[]
  for i in range(Mat_Cont_y.shape[1]):
      if (Mat_Cont_y[:,i]==np.zeros(Mat_Cont_y.shape[0])).all():
        supp_y.append(i)

  Mat_Cont_y=np.delete(Mat_Cont_y,supp_y, axis=1)

  #Calcul des intervalles d'appartenance des variables
  bounds=deepcopy(Profs_Jours_Salles_Disc_)

  for i in range(len(bounds)):
      
      if   float(bounds[i])>b6_[i]:
            (bounds[i])=b6_[i]

  bounds=np.delete(bounds,supp_y)

  #Calcul du vecteur b (Mat_Cont.X <= b)
  b1=np.ones(( len(Profs)*len(Salles_Jours[0]) ))
  b2=np.ones(( len(Salles)*len(Salles_Jours[0]) ))
  b_cap=np.array([nb_tot_eleve])
  b4_2=np.ones((len(Heures_min_profs)))
  for i in range(len(b4_2)):
      b4_2[i]=-Heures_min_profs[i]
  b4_1=np.ones((len(Disc)))
  for i in range(len(b4_1)):
      b4_1[i]=-nbr_min_éléve[i]
  
  b_Max_Disc=np.ones((len(max_Filiere)))
  for i in range(len(b_Max_Disc)):
    b_Max_Disc[i]=max_Filiere[i]

  b_f_y=np.concatenate((b1,b2))
  b_f_y=np.concatenate((b_f_y,b4_2))
  b_f=np.concatenate((b_cap,b4_1))
  b_f=np.concatenate((b_f,b_Max_Disc))

  #On ne laisse que les étiquettes restantes
  Tag[:] = [ item for i,item in enumerate(Tag) if i not in supp_y ]

  Tag_y[:] = [ item for i,item in enumerate(Tag_y) if i not in supp_y ]


  ######################### pulp #########################
  Tag_xalpha[:] = [ item for i,item in enumerate(Tag_xalpha) if i not in supp ]

  Tag_xw[:] = [ item for i,item in enumerate(Tag_xw) if i not in supp ]

  Tag_yalpha[:] = [ item for i,item in enumerate(Tag_yalpha) if i not in supp ]

  Tag_yw[:] = [ item for i,item in enumerate(Tag_yw) if i not in supp ]
  #########################################################
    



###############################        ajout des couts dans la fonction objectif   ###########################################

  ##############  preparation input 
  rev_elev_cour= []  
  for i in range ( len(list_disciplines_)) :
    for j in range(len(list_disciplines_[i])   ):
        rev_elev_cour.append(rev_elev_disc[i]/   len(list_disciplines_[i])      )

  mult=len(Salles)*len(Salles_Jours[0])*len(Profs_Disc[0])
  mult_1=len(Profs_Disc[0])*len(Salles_Jours[0])

  ###############   couts des salles

  cost_salle=np.zeros((int(len(Profs_Jours_Salles_Disc)/len(Profs))))
  for i in range(len(cout_salles)):
    cost_salle[i*mult_1:i*mult_1+mult_1]=cout_salles[i]
  cost_salles=deepcopy(cost_salle)
  for i in range(len(Profs)-1):
    cost_salles=np.concatenate((cost_salles,cost_salle))

  cost_salles = np.delete(cost_salles,supp,axis=0)


  ###############   couts des profs

  cost_prof=np.zeros((len(Profs_Jours_Salles_Disc)))
  for i in range(len(cout_prof)):
    cost_prof[i*mult:i*mult+mult]=cout_prof[i]

  cost_prof = np.delete(cost_prof,supp,axis=0)



  ###############  revenues des eleves

  rev_cours=deepcopy(rev_elev_cour)
  for i in range(int(len(Profs_Jours_Salles_Disc)/len(cours_))-1):
    rev_cours=np.concatenate((rev_cours,rev_elev_cour))

  rev_cours=np.delete(rev_cours,supp,axis=0)


################################################################################################################


  ############################
  #
  # 4 - CALCUL DU SIMPLEXE
  #
  ############################


       
  import pulp


  #Choix du solveur
  solver = pulp.getSolver('PULP_CBC_CMD')
  
  solver_list = pulp.listSolvers() #Liste des solveurs disponibles : ['GLPK_CMD', 'PULP_CBC_CMD', 'COIN_CMD'] (solveur par défaut est : 'PULP_CBC_CMD' qui peut produire des solutions incohérentes si on a un conflit de contraintes)
  model = pulp.LpProblem("My LP Problem", pulp.LpMaximize)#On choisit maximiser comme premier paramètre car on veut maximiser le nombre d'élèves et CBC(Branch&Cut) comme solveur car on traite des variables entières

  #x=[model.add_var(name=Tag[i], var_type=INTEGER, lb=0, ub=bounds[i]) for i in range(Mat_Cont.shape[1])]
  x=[pulp.LpVariable(name=Tag[i], cat='Integer', lowBound=0, upBound=bounds[i]) for i in range(Mat_Cont.shape[1])] #variable entière représentant le nombre d'élèves dans chaque variable
  y = [pulp.LpVariable(name=Tag_y[i], cat='Binary') for i in range(Mat_Cont_y.shape[1])] #variable binaire de décision (prendre ou pas) de chaque variable

  x_ = [pulp.LpVariable(name=Tag_xw[i],cat='Binary', lowBound=0)for i in range(Mat_Cont.shape[1])] #variable nécessaire pour la génération de solutions multiples 
  xa_ = [pulp.LpVariable(name=Tag_xalpha[i],cat='Binary')for i in range(Mat_Cont.shape[1])] #Idem

  y_ = [pulp.LpVariable(name=Tag_yw[i],cat='Binary', lowBound=0)for i in range(Mat_Cont_y.shape[1])] #Idem
  ya_ = [pulp.LpVariable(name=Tag_yalpha[i],cat='Binary')for i in range(Mat_Cont_y.shape[1])] #Idem

  #teta = model.add_var(var_type=CONTINUOUS, lb=0, ub=1) 
  #Ajout des contraintes à partir des matrices Mat_Cont et Mat_Cont_y, b_f et b_f_y
  for i in range(Mat_Cont.shape[0]):
      model+=( pulp.lpSum(Mat_Cont[i][j]*x[j] for j in range(Mat_Cont.shape[1])) <= b_f[i] )

  for i in range(Mat_Cont_y.shape[0]):
      model+= (pulp.lpSum(Mat_Cont_y[i][j]*y[j] for j in range(Mat_Cont_y.shape[1])) <= b_f_y[i] )

  M=100000
  for i in range(Mat_Cont_y.shape[1]):
      model+=(( min_par_seance -(M*(1-y[i])) <= x[i]))  #Si y=0 la contrainte devient inutile (x>0 et x>-M). Si y=1, x>=min_par_séance 
      model+=(( x[i] <= (y[i]*float(bounds[i]))))  #Si y=0, x=0 et si y=1, x <= bounds  

  #Calcul de la moyenne des abonnements par semaine    
  moyenne_abonnements=0
  for i in (rev_elev_disc):
    moyenne_abonnements += i
  moyenne_abonnements = moyenne_abonnements/len(rev_elev_disc)
  moyenne_abonnements_semaine = moyenne_abonnements/nb_semaines

  #Fonction objective : maximiser le nombre d'élèves et minimiser les dépenses
  if budget == 0 :
    model += (pulp.lpSum( x[i]+  x[i]*( moyenne_abonnements_semaine + rev_cours[i]   )- y[i]*(  cost_salles[i] + cost_prof[i]   ) for i in range(Mat_Cont_y.shape[1]))), "Profit"   #Fonction objective : maximiser le nombre d'élèves x.

  else :
    #Contrainte limitante les dépenses par le budget fournit
    C_budget=np.delete(C_budget,supp_y, axis=1)
    model += (pulp.lpSum(C_budget[0][j]*y[j] for j in range(Mat_Cont_y.shape[1])) <= budget )

    #Fonction objective : maximiser le nombre d'élèves et maximiser les dépenses
    model += (pulp.lpSum( x[i]+  x[i]*( moyenne_abonnements_semaine + rev_cours[i]   )+ y[i]*(  cost_salles[i] + cost_prof[i]   ) for i in range(Mat_Cont_y.shape[1]))), "Profit"   


  time_limit=5   # in seconds
  model.solve(pulp.COIN(maxSeconds=time_limit)) #Résolution du problème

  
  # Génération de plusieurs solutions
  M=100000 #Variable très grande nécessaire pour la linéarisation de la nouvelle contrainte en valeur absolue (somme de valeur absolue (x_j - x0_j) <= 1)

  #Variable sols prend les valeurs de x et y trouvés pour chaque solution
  sols=[]
  for i in range(len(Tag)):
    sols.append(x[i].varValue)
  for i in range(len(Tag_y)):
      sols.append(y[i].varValue)

  #Variable all_sols récupère toutes les valeurs trouvés de x de toutes les solutions
  all_sols=[]
  all_sols.append(sols[0:Mat_Cont.shape[1]])
  #list_nb_tot_eleves=[nb_tot_eleves]
  # vfo=model.objective.value()  #valeur de la fonction objective trouvée
  vfo=0
  for i in range(len(Tag)):
    vfo += x[i].varValue
  #vfo=nb_tot_eleves
  if model.status== -1:
    vfo=0

  #Algorithme de génération de solutions mutliples
  nbsols=1
  vfo_pl=vfo
  while (vfo_pl==vfo) and (nbsols < nb_max_sol) and (model.status != -1) and (vfo != 0) :
      for i in range(Mat_Cont.shape[1]):
          model+=((x[i]-sols[i]) + (1-xa_[i])*M >= x_[i] )
          model+=((sols[i]-x[i]) + xa_[i]*M >= x_[i])
      for j in range(Mat_Cont_y.shape[1]):
          model+=((y[j]-sols[j+Mat_Cont.shape[1]]) + (1-ya_[j])*M >= y_[j])
          model+=((sols[j+Mat_Cont.shape[1]]-y[j]) +  ya_[j]*M >=y_[j])
      model+=(pulp.lpSum(x_[j]+y_[j]  for j in range(Mat_Cont.shape[1])) >= 1) #+ teta
 
      try:
         model.solve(pulp.COIN(maxSeconds=time_limit)) #Résolution du problème
      except:
        print("pulp error")
      
      # model.solve(solver)
      sols=[]
      vfo_pl=0
      for i in range(len(Tag)):
        vfo_pl += x[i].varValue
        # vfo_pl=model.objective.value()
  #    for v in model.vars:
  #      if v.x>1:
  #          vfo+=v.x
      for i in range(len(Tag)):
          sols.append(x[i].varValue)
      for i in range(len(Tag_y)):
          sols.append(y[i].varValue)
      if (sols[0:Mat_Cont.shape[1]] not in all_sols) :   
        all_sols.append(sols[0:Mat_Cont.shape[1]])
  #    list_nb_tot_eleves.append(vfo)
      nbsols+=1

  logger.debug(" Fin du calcul  du solver pulp ")


  #cout_salles=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #cout_prof=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #rev_elev=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #
  #cout_sol=[0]*len(list_res_tab)
  #for i in range(len(list_res_tab)):
  #    for j in range(len(list_res_tab[i])):
  #        ind=Salles_s.index(list_res_tab[i][j][2])
  #        ind2=prof.index(list_res_tab[i][j][0])
  #        ind3=cour.index(list_res_tab[i][j][1]* float(list_res_tab[i][j][4]))
  #        cout_sol[i]+=cout_salles[ind]
  #        cout_sol[i]+=cout_prof[ind2]
  #        cout_sol[i]+=cout_elev[ind3]
  

  #Ajout de l'étiquette associé à x pour l'affichage des solutions
  Jours_c=[  Jours[i] + str(k+1)  for i in range(len(Jours))   for k in range(nb_max_cr)]    

  list_res=[  []  for i in range(len(all_sols)) ]
          
  for i in range(len(all_sols)):
      for j in range(len(all_sols[i])):
          if all_sols[i][j]!=0:
              list_res[i].append(Tag[j] + str(all_sols[i][j]))

  list_res_tab=[]
  for i in range(len(list_res)):
      list_res_tab.append([])
    

  for i in range(len(list_res)):
      for j in range(len(list_res[i])):
          list_res_tab[i].append([])

  for i in range(len(list_res)):
      for j in range(len(list_res[i])):
          list_res_tab[i][j]=list_res[i][j].split()
    
  salles_s_res=deepcopy(Salles_s)
  #Salles_s_res.append('')


  for i in range(len(list_res)):
      for j in range(len(list_res[i])):
          list_res[i][j].split()

 ##########################################   calcul nbr eleves par discip et nbr heures par discip  ##########################################""
  long_disc=[0]*(len(List_filieres)+1)
  
  for d in range(len(List_filieres)):
      long_disc[d+1]=len(List_filieres[d])+long_disc[d] 



  nb_eleves_discipline = np.zeros((len(list_res_tab),len(List_filieres)))
  nb_eleves_discipline = nb_eleves_discipline.tolist()

  nbh_par_fil = np.zeros((len(list_res_tab),len(List_filieres)))
  nbh_par_fil = nbh_par_fil.tolist()
  d=0
  while (d < len(List_filieres)):

    for i in range(len(list_res_tab)):
        for j in range(len(list_res_tab[i])):
                if (long_disc[d] <= cour.index(list_res_tab[i][j][1]) < long_disc[d+1]):

                            nbh_par_fil[i][d]+=1
                            nb_eleves_discipline[i][d]+=float(list_res_tab[i][j][4])
    d+=1
  nbh_total=[0]*len(list_res_tab)
  for i in range(len(list_res_tab)):
      for j in range(len(List_filieres)):
          nbh_total[i]+=    nbh_par_fil[i][j]   #nbr total d'eleves
#################################################################################################################################################



  #cout_salles=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #cout_prof=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #rev_elev=[28.5 , 75 , 55 , 25 , 70 , 25 , 0]
  #
  #cout_sol=[0]*len(list_res_tab)
  #for i in range(len(list_res_tab)):
  #    for j in range(len(list_res_tab[i])):
  #        ind=Salles_s.index(list_res_tab[i][j][2])
  #        ind2=prof.index(list_res_tab[i][j][0])
  #        ind3=cour.index(list_res_tab[i][j][1]* float(list_res_tab[i][j][4]))
  #        cout_sol[i]+=cout_salles[ind]
  #        cout_sol[i]+=cout_prof[ind2]
  #        cout_sol[i]+=cout_elev[ind3]
  

 ##############################################  calcul des couts   ##################################################
  
  # cout_salles=[75]
  # cout_prof=[20,57]


  cout_sol=[0]*len(list_res_tab)
  revenue_sol=[0]*len(list_res_tab)
  for i in range(len(list_res_tab)):
     for j in range(len(list_res_tab[i])):

         ind=Salles_s.index(list_res_tab[i][j][2])
         ind2=Profs.index(list_res_tab[i][j][0])
         ind3=cour.index( list_res_tab[i][j][1] )


         cout_sol[i]+=cout_salles[ind]
         cout_sol[i]+=cout_prof[ind2] 


         revenue_sol[i]+=rev_elev_cour[ind3] * float(list_res_tab[i][j][4]) / nb_semaines


  # cout_sol = [ cout_salles[Salles_s.index(list_res_tab[i][j][2])] + cout_prof[Profs.index(list_res_tab[i][j][0])] for i in range(len(list_res_tab)) for j in range(len(list_res_tab[i]))   ]


  logger.debug("\n\n***********************************************************")
  logger.debug("rev_elev_disc")
  logger.debug(rev_elev_disc)
  logger.debug("***********************************************************\n\n")

  logger.debug("\n\n***********************************************************")
  logger.debug("rev_elev_cour")
  logger.debug(rev_elev_cour)
  logger.debug("***********************************************************\n\n")

  logger.debug("\n\n***********************************************************")
  logger.debug("revenue_sol")
  logger.debug(revenue_sol)
  logger.debug("***********************************************************\n\n")

  logger.debug("\n\n***********************************************************")
  logger.debug("cout_sol")
  logger.debug(cout_sol)
  logger.debug("nbh_par_fil")
  logger.debug(nbh_par_fil)
  logger.debug("nb_eleves_discipline")
  logger.debug(nb_eleves_discipline) 
  logger.debug("***********************************************************\n\n")

########################################################################################################################


  score_disc,score_prof,score_contigue = ro.scores.score_contigue(List_filieres,list_res_tab,cour,niveaux_ordre)

  score_disparite = ro.scores.score_disparité(List_filieres,list_res_tab,cour)


  ro.output.createXML(list_res_tab,Salles_Jours,Jours_c,salles_s_res,score_disparite,score_contigue,score_disc,score_prof,emplois_path)

  json_=ro.output.createJSON(list_res_tab,score_disparite,score_contigue,nb_eleves_discipline,vfo,nbh_par_fil,discs_,nbh_total,cout_sol,revenue_sol)


  return(json_)
