import os
from app import db


# from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import JSONB, ARRAY

import datetime
from sqlalchemy import Column, Integer, DateTime
class answer(db.Model):      # object answer map database table tds_roc_answers
    __tablename__ =  "tds_roc_answers"

    id = db.Column(db.Integer, primary_key=True)
    inserted_at=db.Column(  db.DateTime,default=datetime.datetime.utcnow)   
    updated_at=db.Column(  db.DateTime,default=datetime.datetime.utcnow)    
    data = db.Column( JSONB )
    similarity_score = db.Column(db.Integer()  )
    tds_roc_question_id = db.Column(db.Integer()  )

    def __init__(self,data,tds_roc_question_id): #similarity_score

        # self.similarity_score = similarity_score
        self.data = data
        self.tds_roc_question_id = tds_roc_question_id

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def serialize(self):
        return {
            'id': self.id, 
            # 'similarity_score': self.similarity_score,
            'data': self.data,
            'tds_roc_question_id': self.tds_roc_question_id,

        }


class Question(db.Model):   # object questions map database table tds_roc_questions
    __tablename__ =  "tds_roc_questions"

    id = db.Column(db.Integer, primary_key=True)
    inserted_at=db.Column(  db.DateTime,default=datetime.datetime.utcnow)   
    updated_at=db.Column(  db.DateTime,default=datetime.datetime.utcnow)    
    discipline = db.Column((JSONB) )
    teachers = db.Column((JSONB) )
    teaching_slots = db.Column((JSONB) )
    other_constraints = db.Column((JSONB) )
   
    # we do not need the class fuctions  because we only read from tds_roc_questions

 
 
 
        


class levels(db.Model):   # object questions map database table tds_roc_questions
    __tablename__ =  "tds_roc_levels"

    id = db.Column(db.Integer, primary_key=True)
    name=db.Column(  db.String(32)  )   
    numero = db.Column(db.Integer()  )

   
    # we do not need the class fuctions  because we only read from tds_roc_questions

 

