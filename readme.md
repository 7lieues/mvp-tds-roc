app.py  contient le serveur flask qui ecoute sur <nom_serveur>:8100/emploi :
ce serveur recoit l'ID de RECORD du table tds_roc_questions qui contient l'input du model recherche opérationnelle, fetch ce record puis fournie un output et l'enrigistre dans le table tds_roc_answers


models.py       fait le mapping des table tds_roc_questions et tds_roc_answers en objet pour l'ORM SQLalchemy

config.py       Des configs necessaire pour la connexion au base de données


ro.main.py      prepare les variable et les contraintes(contraintes.py) à entré au modele RO puis lance ce modéle pour trouver la liste des solutions, et enfin lance scores.py et output.py

ro.scores.py        parcoure la liste des solution et affecter des scores a chaque solutions selon des différents critéres

ro.output.py       parcoure la liste des solutions trouver et les la liste des scores et crée le dectionnaire qui va etre renvoyer comme OUTPUT


ro.contrainte.py   Crée les contrainte que le model RO doit respecter.


app.py appel ro.main.py
ro.main.py appel ro.scores.py, ro.output.py, ro.contrainte.py 